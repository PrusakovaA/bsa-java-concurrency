create or replace function diff_percent(b bigint) returns decimal as $$
declare n bigint;
declare amount bigint;
BEGIN
    amount := 0;
    for n in 1..64 loop
        amount := amount + (n & 1);
        n = i >> (n - 1);
    end loop;
    return amount/cast(64 as double);
end
$$ language plpgsql;