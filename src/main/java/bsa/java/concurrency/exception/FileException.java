package bsa.java.concurrency.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileException extends RuntimeException{
    public FileException(String message) {
        super(message);
        log.error("File is broken");
    }
}
