package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class FileSystemService implements FileSystem {

    @Value("${fs.path}")
    private String path;


    @Override
    public CompletableFuture<String> saveFile(String name, byte[] file) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        return CompletableFuture.supplyAsync(() -> {
            try {
                if (!Files.exists(Path.of(path))) {
                    Files.createDirectory(Path.of(path));
                }
                var pathToFile = Path.of(path + File.separator + name);
                var os = Files.newOutputStream(pathToFile);
                os.write(file);
                return Arrays.toString(file);
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }, executor);

    }

    @Override
    public CompletableFuture<Void> deleteImg(String img) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        return CompletableFuture.runAsync(() -> {
            new File(path + "/" + img + ".jpg").delete();
        }, executor);
    }

    @Override
    public CompletableFuture<Void> deleteAllImg() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        var result =  CompletableFuture.runAsync(() -> {
            try {
                Files.walk(Path.of(path)).map(Path::toFile).forEach(File::delete);
                new File(path).delete();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }, executor);
        executor.shutdown();
        return result;
    }
}
