package bsa.java.concurrency.fs;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CompletableFuture;
public interface FileSystem {

    CompletableFuture<String> saveFile(String path, byte[] file);
    CompletableFuture<Void> deleteImg(String img);
    CompletableFuture<Void> deleteAllImg();
}
