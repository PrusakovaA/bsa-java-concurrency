package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.FileException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxSize;

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> uploadSize(MaxUploadSizeExceededException e) {
        log.error("Uploaded file size");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Uploaded file size must be less than " + maxSize);
    }


    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        imageService.batchUploadImages(files);
        log.info("Batch upload request");
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> searchMatches(@RequestParam("image") MultipartFile file, @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        try {
            log.info("Match search request");
            var response = imageService.searchMatches(file, threshold);
            if (response.isEmpty()) {
                log.info("No matches found ");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Error");
            }
            log.info("Match search request done");
            return ResponseEntity.ok(response);
        } catch (FileException e) {
            log.error("Match search error:  " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error " + e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        log.info("Delete image with id " + imageId + " request");
        imageService.deleteImage(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        log.info("Purge images request");
        imageService.deleteAllImages();
    }
}
