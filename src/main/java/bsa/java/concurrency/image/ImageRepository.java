package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(value = "select cast(id as varchar) as imageId, 1 - diff_percent(:hash # hash) as matchPercent," +
            "url as imageUrl from images where matchPercent >= :threshold", nativeQuery = true)
    List<SearchResultDTO> searchMatches(long hash, double threshold);
}
