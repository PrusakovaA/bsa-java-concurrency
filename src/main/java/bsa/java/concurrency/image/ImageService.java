package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.FileException;
import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.hash.dHash;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ImageService {

    @Autowired
    private FileSystemService fileSystemService;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private AsyncService asyncService;

    public void batchUploadImages(MultipartFile[] files) {
        for (var multipartFile : files) {
            try {
                var file = multipartFile.getBytes();
                String name = multipartFile.getOriginalFilename();
               asyncService.handleImage(file,name);
            } catch (IOException e) {
                throw new FileException(e.getMessage());
            }
        }

    }

    public List<SearchResultDTO> searchMatches(MultipartFile  multipartFile, double threshold) {
        try {
            var file = multipartFile.getBytes();
            String name = multipartFile.getOriginalFilename();
            long hash = dHash.calculateHash(file);
            var result = imageRepository.searchMatches(hash, threshold);
            if (result.isEmpty()) {
                writeFile(file, name, hash);
            }
            return result;
        } catch (IOException e) {
            throw new FileException(e.getMessage());
        }
    }

    private void writeFile(byte[] file, String name, long hash) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CompletableFuture.runAsync(() -> {
            UUID id = UUID.randomUUID();
            try {
                String url = fileSystemService.saveFile(id.toString() + name, file).get();
                imageRepository.save(new Image(id, hash, url));
            } catch (InterruptedException | ExecutionException e) {
                Thread.currentThread().interrupt();
            }
        }, executor);
        executor.shutdown();
    }

    public void deleteImage(UUID imageId) {
        fileSystemService.deleteImg(imageId.toString());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CompletableFuture.runAsync(()-> imageRepository.deleteById(imageId), executor);
        executor.shutdown();
    }

    public void deleteAllImages() {
        fileSystemService.deleteAllImg();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CompletableFuture.runAsync(() -> imageRepository.deleteAll(), executor);
        executor.shutdown();
    }
}
