package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.hash.dHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class AsyncService {

    @Autowired
    private FileSystemService fileSystemService;

    @Autowired
    private ImageRepository imageRepository;


    @Async
    CompletableFuture<Void> handleImage(byte[] file, String name) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        UUID id = UUID.randomUUID();
        var hash = CompletableFuture.supplyAsync(() -> dHash.calculateHash(file), executor);
        CompletableFuture<String> fs = fileSystemService.saveFile(name, file);
        try {
            imageRepository.save(new Image(id, hash.get(), fs.get()));
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
        }
        executor.shutdown();
        return null;
    }
}
