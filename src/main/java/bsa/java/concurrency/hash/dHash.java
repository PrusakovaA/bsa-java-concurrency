package bsa.java.concurrency.hash;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

public class dHash {

    public static long calculateHash(byte[] image) {
        try {
            var img = ImageIO.read(new ByteArrayInputStream(image));
            return calculateDHash(processedImage(img));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static BufferedImage processedImage(BufferedImage image) {
        int width = 9, height = 9;
        var result = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        var output = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);

        output.getGraphics().drawImage(result, 0, 0, null);

        return output;
    }

    private static int calculateBrightness(int rgb) {
        return rgb & 0b11111111;
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                var current = calculateBrightness(processedImage.getRGB(i, j));
                var prev = calculateBrightness(processedImage.getRGB(i - 1, j - 1));
                if (current > prev) {
                    hash |= 1;
                }
                hash <<= 1;
            }
        }

        return  hash;
    }


}
